function add_error_message(elem, message, with_err) {
  var err_elem = document.createElement('p');
  err_elem.className += " error-box";
  err_elem.appendChild(document.createTextNode(message));
  elem.appendChild(err_elem);
}

function add_posts(elem, blog_url, posts) {
  posts.forEach(function (p) {
    var base_url = blog_url;
    if(!base_url) {
      base_url = p['base_url'];
    }

    var post_link = document.createElement('a');
    post_link.href = base_url + p['slug'];
    var t = document.createElement('p');
    t.className += " miniheader";
    t.appendChild(document.createTextNode(p['title']));
    post_link.appendChild(t);

    var d = document.createElement('p');
    var text = p['body'];
    text = text.substring(0, text.indexOf("<!--more-->"));
    d.appendChild(document.createTextNode(text));

    elem.appendChild(post_link);
    elem.appendChild(d);
    elem.appendChild(document.createElement('hr'));
  });
}

function sort_posts_by_created(posts) {
  var sorted = posts.sort( function (a, b) {
    if(a.created < b.created) { return 1; }
    if(a.created > b.created) { return -1; }
    return 0;
  });

  return sorted;
}

function add_blog(elem, blog) {
  var blog_elem = document.createElement('div');
  var blog_link = document.createElement('a');
  blog_link.href = blog['url'];
  var info = document.createElement('h2');
  info.appendChild(document.createTextNode("Blog: " + blog['title']));
  blog_link.appendChild(info);

  var desc = document.createElement('p');
  desc.appendChild(document.createTextNode(blog['description']));

  blog_elem.appendChild(blog_link);
  blog_elem.appendChild(desc);

  add_posts(blog_elem, blog['url'], blog['posts']);

  elem.appendChild(blog_elem);
}

function make_blogs(elem, blogs) {
  blogs.forEach(function (b) {
    add_blog(elem, b);
  });
}

function combine_blogs(blogs) {
  var posts = [];
  blogs.forEach(function (b) {
    // Push the blog URL down into the post now
    b['posts'].forEach(function (p) { p['base_url'] = b['url']; });

    posts = posts.concat(b['posts']);
  });
  return posts;
}

function get_blogs() {
  var hdrs = new Headers();
  hdrs.append('Content-Type', 'application/json');

  return fetch("https://write.as/api/collections/dar13/posts")
    .then(response => {
      if(!response.ok) {
        console.log("response.ok == False");
        throw new Error("Network response, not OK!");
      }
      return response.json();
    })
    .catch(error => {
      console.log("fetch threw: " + error);
      throw error;
    });
}
