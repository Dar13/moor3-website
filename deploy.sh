#!/bin/sh
if [ $# -eq 0 ]
then
  echo "No arguments supplied!"
  echo "Expecting path to deploy to!"
  echo "Usage: ./deploy.sh <path_to_data_www>"
  exit 1
fi

if [ -d "$1" ]; then
  echo "Target directory exists, removing for clean deploy"
  rm -rf "$1"
fi

mkdir -p "$1"
cp -r ./main/* "$1/"
